package com.herocorp.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "guilde")
public class Guilde {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idguilde;

    @Column(name="nom")
    private String nom;

    @Column(name="argent")
    private int argent;

    @Column(name="salaire_S")
    private int salaire_S;

    @Column(name="salaire_A")
    private int salaire_A;
    
    @Column(name="salaire_B")
    private int salaire_B;
    
    @Column(name="salaire_C")
    private int salaire_C;
    
    @Column(name="salaire_D")
    private int salaire_D;
    
    public String getNom () {
    	return nom;
    }
    
    public int getS () {
    	return salaire_S;
    }
    
    public int getA () {
    	return salaire_A;
    }
    
    public int getB () {
    	return salaire_B;
    }
    
    public int getC () {
    	return salaire_C;
    }
    
    public int getD () {
    	return salaire_D;
    }
}
