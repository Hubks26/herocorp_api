package com.herocorp.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "item")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long iditem;
	
	@Column(name="nom")
    private String nom;

    @Column(name="type_item")
    private String type_item;

    @Column(name="url_image")
    private String url_image;
    
    @Column(name="bonus_mana")
    private int bonus_mana;

    @Column(name="bonus_vie")
    private int bonus_vie;
    
    @Column(name="force_physique")
    private int force_physique;

    @Column(name="puissance_magique")
    private int puissance_magique;
    
    @Column(name="defense_physique")
    private int defense_physique;
    
    @Column(name="defense_magique")
    private int defense_magique;
    
    public String getNom () {
    	return nom;
    }
    
    public String getType () {
    	return type_item;
    }
    
    public String getUrl () {
    	return url_image;
    }
    
    public int getForce () {
    	return force_physique;
    }
    
    public int getPuissance () {
    	return puissance_magique;
    }
    
    public int getMana () {
    	return bonus_mana;
    }
    
    public int getVie () {
    	return bonus_vie;
    }
    
    public int getDefPhysique () {
    	return defense_physique;
    }
    
    public int getDefMagique () {
    	return defense_magique;
    }
    
}
