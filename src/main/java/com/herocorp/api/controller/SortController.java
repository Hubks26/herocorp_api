package com.herocorp.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.herocorp.api.model.Sort;
import com.herocorp.api.services.SortService;

@RestController

public class SortController {
	
	@Autowired
	SortService sortService;
	
	@GetMapping("/sorts")
    public Iterable<Sort> getSorts() {
        return sortService.getSorts();
    }
}
