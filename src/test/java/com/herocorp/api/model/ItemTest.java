package com.herocorp.api.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.herocorp.api.services.ItemService;

@SpringBootTest
public class ItemTest {
	
	@Autowired
	ItemService itemService;

	@Test
	void getTest () {
		Optional<Item> item = itemService.getItem((long) 1);
		String nomAttendu = "Epee";
		String typeAttendu = "Arme Physique";
        String urlAttendu = "image.com";
		int forceAttendu = 2;
		int puissanceAttendu = 0;
		int manaAttendu = 0;
        int vieAttendu = 0;
        int defPhysiqueAttendu = 0;
        int defMagiqueAttendu = 0;
		assertEquals(item.get().getNom(), nomAttendu);
		assertEquals(item.get().getType(), typeAttendu);
		assertEquals(item.get().getUrl(), urlAttendu);
		assertEquals(item.get().getForce(), forceAttendu);
		assertEquals(item.get().getPuissance(), puissanceAttendu);
        assertEquals(item.get().getMana(), manaAttendu);
        assertEquals(item.get().getVie(), vieAttendu);
        assertEquals(item.get().getDefPhysique(), defPhysiqueAttendu);
        assertEquals(item.get().getDefMagique(), defMagiqueAttendu);
	}
}