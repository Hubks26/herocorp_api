package com.herocorp.api.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.herocorp.api.dao.SortDao;
import com.herocorp.api.model.Sort;

@Service
public class SortService {
	@Autowired
    private SortDao sortDao;

    public Optional<Sort> getSort(final Long id) {
        return sortDao.findById(id);
    }

    public Iterable<Sort> getSorts() {
        return sortDao.findAll();
    }
}
