package com.herocorp.api.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.herocorp.api.services.SortService;

@SpringBootTest
public class SortTest {
	
	@Autowired
	SortService sortService;

	@Test
	void getTest () {
		Optional<Sort> sort = sortService.getSort((long) 1);
		String nomAttendu = "sort 1";
		String typeAttendu = "feu";
        String urlAttendu = "image.org";
        String descriptionAttendu = "un super sort";

		assertEquals(sort.get().getNom(), nomAttendu);
		assertEquals(sort.get().getType(), typeAttendu);
		assertEquals(sort.get().getUrl(), urlAttendu);
		assertEquals(sort.get().getDescription(), descriptionAttendu);
	}
}