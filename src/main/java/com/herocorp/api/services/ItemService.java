package com.herocorp.api.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.herocorp.api.dao.ItemDao;
import com.herocorp.api.model.Item;

@Service
public class ItemService {
	@Autowired
    private ItemDao itemDao;

    public Optional<Item> getItem(final Long id) {
        return itemDao.findById(id);
    }

    public Iterable<Item> getItems() {
        return itemDao.findAll();
    }
    
    public Iterable<Item> getItemsByChasseur(Long idchasseur) {
    	Iterable<Integer> ids = itemDao.findByChasseur(idchasseur);
    	List<Long> idsLong = new ArrayList<Long>();
    	Iterator<Integer> it = ids.iterator();
    	while (it.hasNext()) {
    		idsLong.add(Long.valueOf(it.next()));
    	}
    	return itemDao.findAllById(idsLong);
    }
}
