package com.herocorp.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.herocorp.api.model.Item;
import com.herocorp.api.services.ItemService;

@RestController
public class ItemController {
	
	@Autowired
	ItemService itemService;
	
	@GetMapping("/items")
    public Iterable<Item> getItems() {
        return itemService.getItems();
    }

}
