package com.herocorp.api.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.herocorp.api.services.ChasseurService;

@SpringBootTest
public class ChasseurTest {
	
	@Autowired
	ChasseurService chasseurService;

	@Test
	void getTest () {
		Optional<Chasseur> chasseur = chasseurService.getChasseur((long) 1);
		String nomAttendu = "Bryan";
		String rangAttendu = "S";
		int niveauAttendu = 100;
		int argentAttendu = 2500;
		int forceAttendue = 10;
		assertEquals(chasseur.get().getNom(), nomAttendu);
		assertEquals(chasseur.get().getRang(), rangAttendu);
		assertEquals(chasseur.get().getNiveau(), niveauAttendu);
		assertEquals(chasseur.get().getArgent(), argentAttendu);
		assertEquals(chasseur.get().getForce(), forceAttendue);
	}
}
