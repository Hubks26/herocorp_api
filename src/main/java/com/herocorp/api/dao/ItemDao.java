package com.herocorp.api.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.herocorp.api.model.Item;

@Repository
public interface ItemDao extends CrudRepository < Item, Long > {

	
	@Query(value = "SELECT iditem FROM chasseur_item WHERE idchasseur = :idchasseur", nativeQuery = true)
	public Iterable<Integer> findByChasseur(@Param("idchasseur") Long idchasseur);
}
