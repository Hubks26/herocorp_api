package com.herocorp.api.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.herocorp.api.dao.GuildeDao;
import com.herocorp.api.model.Guilde;

@Service
public class GuildeService {
	@Autowired
    private GuildeDao guildeDao;

    public Optional<Guilde> getGuilde(final Long id) {
        return guildeDao.findById(id);
    }

    public Iterable<Guilde> getGuildes() {
        return guildeDao.findAll();
    }
    
    public Optional<Guilde> getGuildeByChasseur (Long idchasseur) {
    	Long idguilde = guildeDao.findByChasseur(idchasseur);
    	return guildeDao.findById(idguilde);
    }
}
