package com.herocorp.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.herocorp.api.model.Chasseur;
import com.herocorp.api.model.Guilde;
import com.herocorp.api.model.Item;
import com.herocorp.api.services.ChasseurService;
import com.herocorp.api.services.GuildeService;
import com.herocorp.api.services.ItemService;

@RestController

public class ChasseurController {
	
	@Autowired
	ChasseurService chasseurService;
	
	@Autowired
	GuildeService guildeService;
	
	@Autowired
	ItemService itemService;
	
	@GetMapping("/chasseurs")
    public Iterable<Chasseur> getChasseurs() {
        return chasseurService.getChasseurs();
    }
	
	@GetMapping("/chasseurs/{idchasseur}/guilde")
	public Optional<Guilde> getGuildeChasseur (@PathVariable("idchasseur") long idchasseur) {
		return guildeService.getGuildeByChasseur(idchasseur);
	}
	
	@GetMapping("/chasseurs/{idchasseur}/items")
	public Iterable<Item> getItemsChasseur (@PathVariable("idchasseur") long idchasseur) {
		return itemService.getItemsByChasseur(idchasseur);
	}
	
	@GetMapping("/chasseurs/{idchasseur}")
	public Optional<Chasseur> getChasseur (@PathVariable("idchasseur") long idchasseur) {
		return chasseurService.getChasseur(idchasseur);
	}
}
