package com.herocorp.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.herocorp.api.model.Chasseur;
import com.herocorp.api.model.Guilde;
import com.herocorp.api.services.ChasseurService;
import com.herocorp.api.services.GuildeService;

@RestController

public class GuildeController {
	
	@Autowired
	GuildeService guildeService;
	
	@Autowired
	ChasseurService chasseurService;
	
	@GetMapping("/guildes")
    public Iterable<Guilde> getGuildes() {
        return guildeService.getGuildes();
    }
	
	@GetMapping("/guildes/{idguilde}/chasseurs")
	public Iterable<Chasseur> getChasseurs (@PathVariable("idguilde") long idguilde) {
		return chasseurService.getChasseursByGuilde(idguilde);
	}
	
	
}
