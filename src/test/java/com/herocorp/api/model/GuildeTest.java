package com.herocorp.api.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.herocorp.api.services.GuildeService;

@SpringBootTest
public class GuildeTest {
	
	@Autowired
	GuildeService guildeService;

	@Test
	void getTest () {
		Optional<Guilde> guilde = guildeService.getGuilde((long) 2);
		String nomAttendu = "Guilde du désert";
		int salaireSAttendu = 9000;
		int salaireAAttendu = 3500;
        int salaireBAttendu = 2500;
        int salaireCAttendu = 2000;
        int salaireDAttendu = 1500;
		assertEquals(guilde.get().getNom(), nomAttendu);
		assertEquals(guilde.get().getS(), salaireSAttendu);
		assertEquals(guilde.get().getA(), salaireAAttendu);
		assertEquals(guilde.get().getB(), salaireBAttendu);
		assertEquals(guilde.get().getC(), salaireCAttendu);
        assertEquals(guilde.get().getD(), salaireDAttendu);
	}
}
