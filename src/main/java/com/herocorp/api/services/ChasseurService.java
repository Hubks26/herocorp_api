package com.herocorp.api.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.herocorp.api.dao.ChasseurDao;
import com.herocorp.api.model.Chasseur;

@Service
public class ChasseurService {
	@Autowired
    private ChasseurDao chasseurDao;
	

    public Optional<Chasseur> getChasseur(final Long id) {
        return chasseurDao.findById(id);
    }

    public Iterable<Chasseur> getChasseurs() {
        return chasseurDao.findAll();
    }
    
    public Iterable<Chasseur> getChasseursByGuilde(Long idguilde) {
    	Iterable<Integer> ids = chasseurDao.findByGuilde(idguilde);
    	List<Long> idsLong = new ArrayList<Long>();
    	Iterator<Integer> it = ids.iterator();
    	while (it.hasNext()) {
    		idsLong.add(Long.valueOf(it.next()));
    	}
    	return chasseurDao.findAllById(idsLong);
    }

}
