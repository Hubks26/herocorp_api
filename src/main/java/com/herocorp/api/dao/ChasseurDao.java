package com.herocorp.api.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.herocorp.api.model.Chasseur;

@Repository
public interface ChasseurDao extends CrudRepository < Chasseur, Long > {
	@Query(value = "SELECT idchasseur FROM chasseur_guilde WHERE idguilde = :idguilde", nativeQuery = true)
	public Iterable<Integer> findByGuilde(@Param("idguilde") Long idguilde);
	
}
