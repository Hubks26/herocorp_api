package com.herocorp.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.herocorp.api.services.SortService;

@SpringBootApplication
public class ApiApplication {
	@Autowired
	public static SortService sortService;
	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
		//System.out.println(sortService.getSorts());
	}

}
