package com.herocorp.api.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.herocorp.api.model.Sort;

@Repository
public interface SortDao extends CrudRepository < Sort, Long > {

}