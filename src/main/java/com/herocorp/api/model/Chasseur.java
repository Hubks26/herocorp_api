package com.herocorp.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "chasseur")
public class Chasseur {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idchasseur;

    @Column(name="nom")
    private String nom;

    @Column(name="rang")
    private String rang;

    @Column(name="niveau")
    private int niveau;

    @Column(name="experience")
    private int experience;
    
    @Column(name="classe")
    private String classe;
    
    @Column(name="argent")
    private int argent;
    
    @Column(name="mana")
    private int mana;

    @Column(name="vie")
    private int vie;
    
    @Column(name="energie")
    private int energie;
    
    @Column(name="force_physique")
    private int force_physique;

    @Column(name="puissance_magique")
    private int puissance_magique;
    
    @Column(name="defense_physique")
    private int defense_physique;
    
    @Column(name="defense_magique")
    private int defense_magique;
    
    public String getNom () {
    	return nom;
    }
    
    public String getRang () {
    	return rang;
    }
    
    public int getNiveau () {
    	return niveau;
    }
    
    public int getArgent () {
    	return argent;
    }
    
    public int getForce () {
    	return force_physique;
    }
    
    public int getPuissance () {
    	return puissance_magique;
    }
    
    public int getMana () {
    	return mana;
    }
    
    public int getVie () {
    	return vie;
    }
    
    public int getEnergie () {
    	return energie;
    }
    
    public int getDefPhysique () {
    	return defense_physique;
    }
    
    public int getDefMagique () {
    	return defense_magique;
    }
    
    
}
