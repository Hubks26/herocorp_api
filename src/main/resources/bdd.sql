DROP TABLE IF EXISTS chasseur CASCADE;
DROP TABLE IF EXISTS item CASCADE;
DROP TABLE IF EXISTS donjon CASCADE;
DROP TABLE IF EXISTS guilde CASCADE;
DROP TABLE IF EXISTS sort CASCADE;
DROP TABLE IF EXISTS chasseur_item CASCADE;
DROP TABLE IF EXISTS chasseur_sort CASCADE;
DROP TABLE IF EXISTS chasseur_guilde CASCADE;
DROP SEQUENCE IF EXISTS idChasseur_seq;
DROP SEQUENCE IF EXISTS idItem_seq;
DROP SEQUENCE IF EXISTS idDonjon_seq;
DROP SEQUENCE IF EXISTS idGuilde_seq;
DROP SEQUENCE IF EXISTS idSort_seq;


CREATE SEQUENCE idChasseur_seq;
CREATE SEQUENCE idItem_seq;
CREATE SEQUENCE idDonjon_seq;
CREATE SEQUENCE idGuilde_seq;
CREATE SEQUENCE idSort_seq;

CREATE TABLE chasseur (
    idChasseur INT NOT NULL DEFAULT nextval
    ('idChasseur_seq'::regclass) PRIMARY KEY UNIQUE,
    nom VARCHAR(20),
    rang VARCHAR(1),
    niveau INT,
    experience INT,
    classe VARCHAR(20),
    argent INT,
    force_physique INT,
    puissance_magique INT,
    mana INT,
    vie INT,
    energie INT,
    defense_physique INT,
    defense_magique INT
);

INSERT INTO chasseur (nom, rang, niveau, experience, classe, argent, force_physique, puissance_magique, mana, vie, energie, defense_physique, defense_magique)
    VALUES ('Bryan', 'S', 100, 0, 'Assassin', 2500, 10, 10, 20, 100, 30, 10, 10);
INSERT INTO chasseur (nom, rang, niveau, experience, classe, argent, force_physique, puissance_magique, mana, vie, energie, defense_physique, defense_magique)
    VALUES ('Alex', 'A', 90, 1340, 'Mage', 2100, 5, 15, 30, 100, 30, 8, 8);

CREATE TABLE item (
    idItem INT NOT NULL DEFAULT nextval
    ('idItem_seq'::regclass) PRIMARY KEY UNIQUE,
    nom VARCHAR(20),
    type_item VARCHAR(20),
    url_image TEXT,
    force_physique INT,
    puissance_magique INT,
    bonus_mana INT,
    bonus_vie INT,
    defense_physique INT,
    defense_magique INT
);

INSERT INTO item (nom, type_item, url_image, force_physique, puissance_magique, bonus_mana, bonus_vie, defense_physique, defense_magique)
    VALUES ('Epee', 'Arme Physique', 'image.com', 2, 0, 0, 0, 0, 0);

INSERT INTO item (nom, type_item, url_image, force_physique, puissance_magique, bonus_mana, bonus_vie, defense_physique, defense_magique)
    VALUES ('Sceptre', 'Arme Magique', 'image.com', 0, 3, 0, 0, 0, 0);

CREATE TABLE donjon (
    idDonjon INT NOT NULL DEFAULT nextval
    ('idDonjon_seq'::regclass) PRIMARY KEY UNIQUE,
    heureFin DATE,
    survie BOOLEAN,
    idChasseur INT REFERENCES chasseur ON DELETE CASCADE
);

CREATE TABLE guilde (
    idGuilde INT NOT NULL DEFAULT nextval
    ('idGuilde_seq'::regclass) PRIMARY KEY UNIQUE,
    nom VARCHAR(20),
    argent INT,
    salaire_S INT,
    salaire_A INT,
    salaire_B INT,
    salaire_C INT,
    salaire_D INT
);

INSERT INTO guilde (nom, argent, salaire_S, salaire_A, salaire_B, salaire_C, salaire_D)
    VALUES ('Guilde 1', 120000, 15000, 4500, 3000, 2000, 1500);
INSERT INTO guilde (nom, argent, salaire_S, salaire_A, salaire_B, salaire_C, salaire_D)
    VALUES ('Guilde du désert', 80000, 9000, 3500, 2500, 2000, 1500);
INSERT INTO guilde (nom, argent, salaire_S, salaire_A, salaire_B, salaire_C, salaire_D)
    VALUES ('Guilde Egalitaire', 75000, 2500, 2500, 2500, 2500, 2500);


CREATE TABLE sort (
    idSort INT NOT NULL DEFAULT nextval
    ('idSort_seq'::regclass) PRIMARY KEY UNIQUE,
    nom VARCHAR(20),
    description_sort TEXT,
    url_image TEXT,
    type_sort VARCHAR(20)
);

INSERT INTO sort (nom, description_sort, url_image, type_sort)
    VALUES ('sort 1', 'un super sort', 'image.org', 'feu');
INSERT INTO sort (nom, description_sort, url_image, type_sort)
    VALUES ('sort 2', 'un autre super sort', 'image.org', 'eau');

CREATE TABLE chasseur_guilde (
    idChasseur INT REFERENCES chasseur ON DELETE CASCADE,
    idGuilde INT REFERENCES guilde ON DELETE CASCADE
);

INSERT INTO chasseur_guilde VALUES (1, 1);
INSERT INTO chasseur_guilde VALUES (2, 2);

CREATE TABLE chasseur_sort (
    idChasseur INT REFERENCES chasseur ON DELETE CASCADE,
    idSort INT REFERENCES sort ON DELETE CASCADE
);

CREATE TABLE chasseur_item (
    idChasseur INT REFERENCES chasseur ON DELETE CASCADE,
    idItem INT REFERENCES item ON DELETE CASCADE
);

INSERT INTO chasseur_item VALUES (1, 1);
INSERT INTO chasseur_item VALUES (1, 2);
INSERT INTO chasseur_item VALUES (2, 2);