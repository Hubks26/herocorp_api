package com.herocorp.api.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.herocorp.api.model.Guilde;

@Repository
public interface GuildeDao extends CrudRepository < Guilde, Long > {
	
	@Query(value = "SELECT idguilde FROM chasseur_guilde WHERE idchasseur = :idchasseur", nativeQuery = true)
	public Long findByChasseur(@Param("idchasseur") Long idchasseur);
	
}
