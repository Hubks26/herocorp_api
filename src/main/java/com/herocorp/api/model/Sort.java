package com.herocorp.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sort")
public class Sort {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idsort;
	
	@Column(name="nom")
    private String nom;

    @Column(name="description_sort")
    private String description_sort;

    @Column(name="url_image")
    private String url_image;

    @Column(name="type_sort")
    private String type_sort;
    
    public String getNom () {
    	return nom;
    }
    
    public String getUrl () {
    	return url_image;
    }
    
    public String getDescription () {
    	return description_sort;
    }
    
    public String getType () {
    	return type_sort;
    }
    
}
